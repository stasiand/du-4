package shop;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class StandartItemTest {
    @Test
    void Constructor() {
        StandardItem car = new StandardItem(1, "toyota", 15000.0f, "passenger", 5);
        assertEquals(1, car.getID());
        assertEquals("toyota", car.getName());
        assertEquals(15000.0f, car.getPrice());
        assertEquals("passenger", car.getCategory());
        assertEquals(5, car.getLoyaltyPoints());
    }

    @Test
    void Copy() {
        StandardItem car = new StandardItem(1, "toyota", 15000.0f, "passenger", 5);
        StandardItem copy = car.copy();
        assertEquals(car, copy);
        assertNotSame(car, copy);
    }

    @Test
    void Equals() {
        StandardItem car1 = new StandardItem(1, "toyota", 15000.0f, "passenger", 5);
        StandardItem car2 = new StandardItem(1, "toyota", 15000.0f, "passenger", 5);
        StandardItem car3 = new StandardItem(2, "BMV", 25000.0f, "racing", 20);
        assertEquals(car1, car2);
        assertNotEquals(car1, car3);
    }
}

