package shop;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class OrderTest {

    @Test
    void Constructor() {
        ShoppingCart shop_cart = new ShoppingCart();
        String name = "Honza";
        String address = "Praha";

        Order order = new Order(shop_cart, name, address);

        assertEquals(shop_cart.getCartItems(), order.getItems());
        assertEquals(name, order.getCustomerName());
        assertEquals(address, order.getCustomerAddress());
        assertEquals(0, order.getState());
    }

    @Test
    void ConstructorWithState() {
        ShoppingCart shop_cart = new ShoppingCart();
        String name = "Honza";
        String address = "Praha";
        int state = 1;

        Order order = new Order(shop_cart, name, address, state);

        assertEquals(shop_cart.getCartItems(), order.getItems());
        assertEquals(name, order.getCustomerName());
        assertEquals(address, order.getCustomerAddress());
        assertEquals(state, order.getState());
    }



    @Test
    void testNullValues() {
        Order order = new Order(null, null, null);

        assertTrue(order.getItems().isEmpty());
        assertNull(order.getCustomerName());
        assertNull(order.getCustomerAddress());
        assertEquals(0, order.getState());
    }
}