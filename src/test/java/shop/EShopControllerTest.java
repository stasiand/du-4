package shop;

import archive.PurchasesArchive;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import storage.*;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

class EShopControllerTest {

    private Storage storage;
    private PurchasesArchive arch;
    private StandardItem item;

    @BeforeEach
    void setUp() {
        storage = mock(Storage.class);
        arch = mock(PurchasesArchive.class);
        item = new StandardItem(1, "chleb", 120.0f, "Potraviny", 3);

        EShopController.startEShop();
        EShopController.setStorage(storage);
        EShopController.setArchive(arch);
    }

    @Test
    void testPurchaseShoppingCart() throws NoItemInStorage {
        ShoppingCart cart = new ShoppingCart();
        cart.addItem(item);
        when(storage.hasItem(item)).thenReturn(true);

        EShopController.purchaseShoppingCart(cart, "Vova", "Palachova 5");

        verify(storage, times(1)).processOrder(any(Order.class));
        verify(arch, times(1)).putOrderToPurchasesArchive(any(Order.class));
    }

    @Test
    void testPurchaseEmptyShoppingCart() throws NoItemInStorage {
        ShoppingCart cart = new ShoppingCart();

        EShopController.purchaseShoppingCart(cart, "Vova", "Palachova 5");

        verify(storage, never()).processOrder(any(Order.class));
        verify(arch, never()).putOrderToPurchasesArchive(any(Order.class));
    }
    @Test
    void testAddItemToCartNotInStorage() {
        Item itemNotInStorage = new StandardItem(2, "Mleko", 200.0f, "Potraviny", 6);
        ShoppingCart cart = new ShoppingCart();

        assertThrows(NoItemInStorage.class, () -> {
            cart.addItem(itemNotInStorage);
            EShopController.purchaseShoppingCart(cart, "Vova", "Palachova 5");
        });
    }
}