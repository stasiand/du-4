package archive;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import shop.*;
import storage.*;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class PurchasesArchiveTest {

    @Test
    void testPrintItemPurchaseStatistics() {
        ItemPurchaseArchiveEntry e = mock(ItemPurchaseArchiveEntry.class);
        when(e.toString()).thenReturn("Item1: 5");

        HashMap<Integer, ItemPurchaseArchiveEntry> item_arch = new HashMap<>();
        item_arch.put(1, e);

        PurchasesArchive arch = new PurchasesArchive(item_arch, new ArrayList<>());

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        System.setOut(new PrintStream(out));

        arch.printItemPurchaseStatistics();
        System.out.println("Actual output: " + out.toString());

        assertTrue(out.toString().contains("Item1: 5"));
    }

    @Test
    void testGetHowManyTimesHasBeenItemSold() {
        ItemPurchaseArchiveEntry e = mock(ItemPurchaseArchiveEntry.class);
        when(e.getCountHowManyTimesHasBeenSold()).thenReturn(5);

        HashMap<Integer, ItemPurchaseArchiveEntry> item_arch = new HashMap<>();
        item_arch.put(1, e);

        Item item = mock(Item.class);
        when(item.getID()).thenReturn(1);

        PurchasesArchive arch = new PurchasesArchive(item_arch, new ArrayList<>());
        int sold_count = arch.getHowManyTimesHasBeenItemSold(item);

        assertEquals(5, sold_count);
    }

    @Test
    void testPutOrderToPurchasesArchive() {
        Item item = mock(Item.class);
        when(item.getID()).thenReturn(1);

        ArrayList<Item> items = new ArrayList<>();
        items.add(item);

        Order order = mock(Order.class);
        when(order.getItems()).thenReturn(items);

        ItemPurchaseArchiveEntry e = mock(ItemPurchaseArchiveEntry.class);

        HashMap<Integer, ItemPurchaseArchiveEntry> item_arch = new HashMap<>();
        item_arch.put(1, e);

        PurchasesArchive arch = new PurchasesArchive(item_arch, new ArrayList<>());
        arch.putOrderToPurchasesArchive(order);

        verify(e, times(1)).increaseCountHowManyTimesHasBeenSold(1);
    }
}