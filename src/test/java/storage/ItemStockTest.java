package storage;

import org.junit.jupiter.api.Test;
import shop.StandardItem;
import static org.junit.jupiter.api.Assertions.*;

class ItemStockTest {

    @Test
    void Constructor() {
        StandardItem proc = new StandardItem(1, "intel i5", 2570.0f, "4x", 7);
        ItemStock proc_stock = new ItemStock(proc);
        assertEquals(proc, proc_stock.getItem());
        assertEquals(0, proc_stock.getCount());
    }

    @Test
    void IncreaseCount() {
        StandardItem proc = new StandardItem(1, "intel i5", 2570.0f, "4x", 7);
        ItemStock proc_stock = new ItemStock(proc);
        proc_stock.IncreaseItemCount(5);
        assertEquals(5, proc_stock.getCount());
    }

    @Test
    void DecreaseCount() {
        StandardItem proc = new StandardItem(1, "intel i5", 2570.0f, "4x", 7);
        ItemStock proc_stock = new ItemStock(proc);
        proc_stock.IncreaseItemCount(5);
        proc_stock.decreaseItemCount(3);
        assertEquals(2, proc_stock.getCount());
    }
}