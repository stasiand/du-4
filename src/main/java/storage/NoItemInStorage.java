package storage;

public class NoItemInStorage extends Exception{
    public NoItemInStorage(String message) {
        super(message);
    }
}
